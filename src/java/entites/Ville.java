/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tpoya
 */
public class Ville {
    private int id;
    private String nom;
    private int idCanton;
    private double coordLat;
    private double coordLong;

    public Ville(int id, String nom, int idCanton, double coordLat, double coordLong) {
        this.id = id;
        this.nom = nom;
        this.idCanton = idCanton;
        this.coordLat = coordLat;
        this.coordLong = coordLong;
    }

    
    
    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public int getIdCanton() {
        return idCanton;
    }

    public double getCoordLat() {
        return coordLat;
    }

    public double getCoordLong() {
        return coordLong;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }
}
