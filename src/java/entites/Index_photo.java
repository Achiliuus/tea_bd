/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tpoya
 */
public class Index_photo {
    private int idPhoto;
    private int idSousCatIndexIcono;

    public Index_photo(int idPhoto, int idSousCatIndexIcono) {
        this.idPhoto = idPhoto;
        this.idSousCatIndexIcono = idSousCatIndexIcono;
    }

    public int getIdPhoto() {
        return idPhoto;
    }

    public int getIdSousCatIndexIcono() {
        return idSousCatIndexIcono;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }
}
