/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author tpoya
 */
public class Personne {
    private int id;
    
    private String nom;
    
    private String prenom;
    
    private String statut;
    
    private String representation;

    public Personne(int id, String nom, String prenom, String statut, String representation) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.statut = statut;
        this.representation = representation;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getStatut() {
        return statut;
    }

    public String getRepresentation() {
        return representation;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }
}
