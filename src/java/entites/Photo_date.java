/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tpoya
 */
public class Photo_date {
    private int id;
    private String jour;
    private String mois;
    private int annee;

    public Photo_date(int id, String jour, String mois, int annee) {
        this.id = id;
        this.jour = jour;
        this.mois = mois;
        this.annee = annee;
    }

    public int getId() {
        return id;
    }

    public String getJour() {
        return jour;
    }

    public String getMois() {
        return mois;
    }

    public int getAnnee() {
        return annee;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }   
}
