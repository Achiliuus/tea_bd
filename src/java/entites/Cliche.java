/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tpoya
 */
public class Cliche {
    private int id;
    private int nombre;
    private double longueur;
    private double largeur;
    private String couleur;
    private String neg_inv;
    private String point_de_vue;

    public Cliche(int id, int nombre, double longueur, double largeur, String couleur, String neg_inv, String point_de_vue) {
        this.id = id;
        this.nombre = nombre;
        this.longueur = longueur;
        this.largeur = largeur;
        this.couleur = couleur;
        this.neg_inv = neg_inv;
        this.point_de_vue = point_de_vue;
    }

    public int getId() {
        return id;
    }

    public int getNombre() {
        return nombre;
    }

    public double getLongueur() {
        return longueur;
    }

    public double getLargeur() {
        return largeur;
    }

    public String getCouleur() {
        return couleur;
    }

    public String getNeg_inv() {
        return neg_inv;
    }

    public String getPoint_de_vue() {
        return point_de_vue;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }
}
