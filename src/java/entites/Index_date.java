/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tpoya
 */
public class Index_date {
    private int idDate;
    private int idPhoto;

    public Index_date(int idDate, int idPhoto) {
        this.idDate = idDate;
        this.idPhoto = idPhoto;
    }

    public int getIdDate() {
        return idDate;
    }

    public int getIdPhoto() {
        return idPhoto;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }
}
