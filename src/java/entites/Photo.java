/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;

/**
 *
 * @author tpoya
 */
public class Photo {
    private int id;
    private int idArticle;
    private String serie;
    private int ville;
    private String description;
    private String note;
    private int personne;
    private int cliche;
    private String remarque;

    public Photo(int id, int idArticle, String serie, int ville, String description, String note, int personne, int cliche, String remarque) {
        this.id = id;
        this.idArticle = idArticle;
        this.serie = serie;
        this.ville = ville;
        this.description = description;
        this.note = note;
        this.personne = personne;
        this.cliche = cliche;
        this.remarque = remarque;
    }

    public int getId() {
        return id;
    }

    public int getIdArticle() {
        return idArticle;
    }

    public String getSerie() {
        return serie;
    }

    public int getVille() {
        return ville;
    }

    public String getDescription() {
        return description;
    }

    public String getNote() {
        return note;
    }

    public int getPersonne() {
        return personne;
    }

    public int getCliche() {
        return cliche;
    }

    public String getRemarque() {
        return remarque;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            System.out.println("Attr : " + field.getName());
            attributes.add(field.getName());
        }
        return attributes;
    }
}
