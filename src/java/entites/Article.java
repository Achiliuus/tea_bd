/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tpoya
 */
public class Article {
    private int id;
    private int article;
    private String discriminant;

    public Article(int id, int article, String discriminant) {
        this.id = id;
        this.article = article;
        this.discriminant = discriminant;
    }

    public int getId() {
        return id;
    }

    public int getArticle() {
        return article;
    }

    public String getDiscriminant() {
        return discriminant;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }
}
