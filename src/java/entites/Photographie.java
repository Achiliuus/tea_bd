/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author couss
 */
public class Photographie {
        
    private int article;
    private String discriminant,
            serie,
            ville,
            sujet,
            description,
            date,
            note,
            idxPers,
            nomFichier;
    private int nbCliche;
    private String tailleCliche,
            c_nb,
            neg_inv;

    public Photographie(int article, String discriminant, String serie, String ville, String sujet, String description, String date, String note, String idxPers, String nomFichier, int nbCliche, String tailleCliche, String c_nb, String neg_inv) {
        this.article = article;
        this.discriminant = discriminant;
        this.serie = serie;
        this.ville = ville;
        this.sujet = sujet;
        this.description = description;
        this.date = date;
        this.note = note;
        this.idxPers = idxPers;
        this.nomFichier = nomFichier;
        this.nbCliche = nbCliche;
        this.tailleCliche = tailleCliche;
        this.c_nb = c_nb;
        this.neg_inv = neg_inv;
    }

    public int getArticle() {
        return article;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public String getDiscriminant() {
        return discriminant;
    }

    public void setDiscriminant(String discriminant) {
        this.discriminant = discriminant;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIdxPers() {
        return idxPers;
    }

    public void setIdxPers(String idxPers) {
        this.idxPers = idxPers;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public int getNbCliche() {
        return nbCliche;
    }

    public void setNbCliche(int nbCliche) {
        this.nbCliche = nbCliche;
    }

    public String getTailleCliche() {
        return tailleCliche;
    }

    public void setTailleCliche(String tailleCliche) {
        this.tailleCliche = tailleCliche;
    }

    public String getC_nb() {
        return c_nb;
    }

    public void setC_nb(String c_nb) {
        this.c_nb = c_nb;
    }

    public String getNeg_inv() {
        return neg_inv;
    }

    public void setNeg_inv(String neg_inv) {
        this.neg_inv = neg_inv;
    }

    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }


}
