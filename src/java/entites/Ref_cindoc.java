/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tpoya
 */
public class Ref_cindoc {
    private int ref;
    private int photo;

    public Ref_cindoc(int ref, int photo) {
        this.ref = ref;
        this.photo = photo;
    }

    public int getRef() {
        return ref;
    }

    public int getPhoto() {
        return photo;
    }
    
    public List<String> getFields() {
        final List<String> attributes = new ArrayList<>();    
        for (final Field field: this.getClass().getFields()) {
            field.setAccessible(true);
            attributes.add(field.getName());
        }
        return attributes;
    }
}
