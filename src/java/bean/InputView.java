package bean;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tpoya
 */
@ManagedBean
public class InputView implements Serializable {
    private String table;
    private List<String> tables;
    
    @PostConstruct
    public void init() {
        table = "Photo";
        
        tables = new ArrayList<>();
        tables.add("Photo");
        tables.add("Ville");
        tables.add("Sujet");
    }
    
    public List<String> getTables() {
        return tables;
    }
    
    public String getTable() {
        return table;
    }

    public void setTableSelected(String table) {
        this.table = table;
    }
}
