/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author couss
 */
@ManagedBean
public class ImageView {
     
    private ArrayList<String> images;
     
    @PostConstruct
    public void init() {
        images = new ArrayList<String>();
        images.add("loiret1.jpg");
        images.add("loiret2.jpg");
        images.add("loiret3.jpg");
        images.add("loiret4.jpg");
        images.add("loiret5.jpg");
        images.add("loiret6.jpg");
        images.add("loiret7.jpg");
        images.add("loiret8.jpg");
    }
 
    public ArrayList<String> getImages() {
        return images;
    }
}