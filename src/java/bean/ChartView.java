package bean;


import entites.Photo;
import entites.Sujet;
import entites.Ville;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tpoya
 */
@ManagedBean
public class ChartView implements Serializable {
    private PieChartModel pieModel;
    private PieChartModel pieModel2;
    private PieChartModel pieModel3;
    private PieChartModel pieModel4;
    private PieChartModel pieModel5;

    
    private BarChartModel barModel;
    
    
    public PieChartModel getPieModel3() {
        return pieModel3;
    }
    
    @PostConstruct
    public void init() {
        pieModel = new PieChartModel();
        pieModel.setShowDatatip(true);
        pieModel.setShowDataLabels(true);
        pieModel.setDataFormat("value");
        pieModel.setDataLabelFormatString("%d");
        pieModel.setLegendPosition("e");
        pieModel.setLegendPlacement(LegendPlacement.OUTSIDE);
        
        pieModel2 = new PieChartModel();
        pieModel2.setShowDatatip(true);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDataFormat("value");
        pieModel2.setDataLabelFormatString("%d");
        pieModel2.setLegendPosition("e");
        pieModel2.setLegendPlacement(LegendPlacement.OUTSIDE);
        
        pieModel3 = new PieChartModel();
        pieModel3.setShowDatatip(true);
        pieModel3.setShowDataLabels(true);
        pieModel3.setDataFormat("value");
        pieModel3.setDataLabelFormatString("%d");
        
        pieModel4 = new PieChartModel();
        pieModel4.setShowDatatip(true);
        pieModel4.setShowDataLabels(true);
        pieModel4.setDataFormat("value");
        pieModel4.setDataLabelFormatString("%d");
        pieModel4.setLegendPosition("e");
        pieModel4.setLegendPlacement(LegendPlacement.OUTSIDE);
        
        pieModel5 = new PieChartModel();
        pieModel5.setShowDatatip(true);
        pieModel5.setShowDataLabels(true);
        pieModel5.setDataFormat("value");
        pieModel5.setDataLabelFormatString("%d");
        pieModel5.setLegendPosition("e");
        pieModel5.setLegendPlacement(LegendPlacement.OUTSIDE);
        
        barModel = new BarChartModel();
 
        ChartSeries noir = new ChartSeries();
        noir.setLabel("Photo Noir&Blanc");
 
        ChartSeries couleur = new ChartSeries();
        couleur.setLabel("Photo Couleur");
        
        barModel.setTitle("Photos en noir et blanc ou couleur en fonction des années");
        barModel.setLegendPosition("e");
        barModel.setLegendPlacement(LegendPlacement.OUTSIDE);
 
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Année");
 
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Couleur");
        yAxis.setMin(0);
        yAxis.setMax(200);
        
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/TEA_BD", "postgres", "postgres")) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select S.nom , count(*) from photo P, categorie_indexicono C, souscat_indexicono S, index_photo I " +
                                                            "where I.idphoto = P.id and S.id = I.idsouscatindexicono and C.id = S.idcatindexicono " +
                                                            "group by S.nom order by count(*) DESC limit 5");
            pieModel.setTitle("Top 5 des sous categories iconographiques les plus représentées");
            while (resultSet.next()) {
                        pieModel.set(resultSet.getString("nom"), resultSet.getInt("count"));
            }
            resultSet = statement.executeQuery("select D.annee,count(*) from photo P, cliche C, index_date I, photo_date D " +
                                                    "where P.cliche = C.id and I.iddate=D.id and I.idphoto=P.id " +
                                                    "and C.couleur = 'nb' group by D.annee");
             while (resultSet.next()) {
                        if(resultSet.getInt("annee") >0){
                            noir.set(resultSet.getInt("annee"), resultSet.getInt("count")   );
                        }
            }
            resultSet = statement.executeQuery("select D.annee,count(*) from photo P, cliche C, index_date I, photo_date D " +
                                                   "where P.cliche = C.id and I.iddate=D.id and I.idphoto=P.id " +
                                                   "and C.couleur = 'couleur' group by D.annee");
            while (resultSet.next()) {
                       if(resultSet.getInt("annee") >0){
                           couleur.set(resultSet.getInt("annee"), resultSet.getInt("count")   );
                       }
            }
            resultSet = statement.executeQuery("select C.nom, count(*) from photo P, categorie_indexicono C, souscat_indexicono S, index_photo I " +
                                                "where I.idphoto = P.id and S.id = I.idsouscatindexicono and C.id = S.idcatindexicono " +
                                                    "and C.nom like '%Architecture%' group by C.nom");
            pieModel2.setTitle("Type d'architecture les plus représentées");
            while (resultSet.next()) {
                        pieModel2.set(resultSet.getString("nom"), resultSet.getInt("count"));
            }
            
            resultSet= statement.executeQuery("select C.nom, count(*) from photo P, ville V, canton C " +
                                                "where P.ville = V.id and C.id = V.idcanton group by C.nom " +
                                                    "order by count(*) desc");
            pieModel3.setTitle("Cantons les plus représentés");
            while (resultSet.next()) {
                        pieModel3.set(resultSet.getString("nom"), resultSet.getInt("count"));
            }
            
            resultSet = statement.executeQuery("select CONCAT(Pe.nom, ' ',Pe.prenom, ' ', Pe.statut), count(P.id) from personne Pe, photo P\n" +
                "where P.personne = Pe.id and CONCAT(Pe.nom, ' ',Pe.prenom, ' ', Pe.statut) != '  ' group by Pe.nom, Pe.prenom, Pe.statut limit 5;");
            pieModel4.setTitle("Top 5 des personnes les plus représentées");
            while (resultSet.next()) {
                        pieModel4.set(resultSet.getString("concat"), resultSet.getInt("count"));
            }
            resultSet = statement.executeQuery("select S.nom , count(*) from photo P, categorie_indexicono C, souscat_indexicono S, index_photo I " +
                    "where I.idphoto = P.id and S.id = I.idsouscatindexicono and C.id = S.idcatindexicono and C.nom like '%religieuse%' " +
                            "group by S.nom order by count(*) DESC limit 5");
            while (resultSet.next()) {
                        pieModel5.set(resultSet.getString("nom"), resultSet.getInt("count"));
            }
            
            barModel.addSeries(noir);
            barModel.addSeries(couleur);
        }catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        
        
    }
    
    public PieChartModel getPieModel() {
        return pieModel;
    }
    
    public BarChartModel getBarModel() {
        return barModel;
    }

    public PieChartModel getPieModel2() {
        return pieModel2;
    }

    public PieChartModel getPieModel4() {
        return pieModel4;
    }

    public void setPieModel4(PieChartModel pieModel4) {
        this.pieModel4 = pieModel4;
    }

    public PieChartModel getPieModel5() {
        return pieModel5;
    }

    public void setPieModel5(PieChartModel pieModel5) {
        this.pieModel5 = pieModel5;
    }
    
    
    
}
