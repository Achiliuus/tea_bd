/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import frames.InsertFrame;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIInput;
import javax.faces.event.ActionEvent;

/**
 *
 * @author tpoya
 */
@ManagedBean
@SessionScoped
public class InsertView implements Serializable{
    private Connection con;

    private String
            refCindoc,
            serie,
            article,
            discriminant,
            ville,
            sujet,
            description,
            date,
            NBP,
            idxPers,
            nomFichier,
            idxIcono,
            nbCliche,
            tailleCliche,
            neg_inv,
            c_nb,
            remarque;
    
    @PostConstruct
    public void init() {
        try {
            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/TEA_BD", "postgres", "postgres");
        } catch (SQLException ex) {
            Logger.getLogger(InsertView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserer() {
        System.out.println(refCindoc + ", " + article + ", " + ville + ", " + sujet + ", " + date + ", " + neg_inv + ", " + c_nb);
        String req = "INSERT INTO photographie VALUES(" + refCindoc + ", " + serie + ", " + article + ", " + discriminant + ", " + ville + ", " + sujet +
                ", " + description + ", " + date + ", " + NBP + ", " + idxPers + ", " + nomFichier + ", " + idxIcono + ", " + nbCliche + ", " + tailleCliche + 
                ", " + neg_inv + ", " + c_nb + ", " + remarque + ")";
        /*
        try {
            Statement stmt = con.createStatement();
            stmt.executeUpdate(req);
        } catch (SQLException ex) {
            Logger.getLogger(InsertView.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }

    public String getRefCindoc() {
        return refCindoc;
    }

    public void setRefCindoc(String refCindoc) {
        this.refCindoc = refCindoc;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getDiscriminant() {
        return discriminant;
    }

    public void setDiscriminant(String discriminant) {
        this.discriminant = discriminant;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNBP() {
        return NBP;
    }

    public void setNBP(String nBP) {
        this.NBP = nBP;
    }

    public String getIdxPers() {
        return idxPers;
    }

    public void setIdxPers(String idxPers) {
        this.idxPers = idxPers;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String NomFichier) {
        this.nomFichier = NomFichier;
    }

    public String getIdxIcono() {
        return idxIcono;
    }

    public void setIdxIcono(String idxIcono) {
        this.idxIcono = idxIcono;
    }

    public String getNbCliche() {
        return nbCliche;
    }

    public void setNbCliche(String nbCliche) {
        this.nbCliche = nbCliche;
    }

    public String getTailleCliche() {
        return tailleCliche;
    }

    public void setTailleCliche(String tailleCliche) {
        this.tailleCliche = tailleCliche;
    }

    public String getNeg_inv() {
        return neg_inv;
    }

    public void setNeg_inv(String neg_inv) {
        this.neg_inv = neg_inv;
    }

    public String getC_nb() {
        return c_nb;
    }

    public void setC_nb(String c_nb) {
        this.c_nb = c_nb;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
    
    public void method(ActionEvent event) {        

        String param = (String) ((UIInput) event.getComponent().findComponent("form:inputRef")).getValue();
        System.out.println("Param: "+param);

    }
    
    public void affiche() {
        System.out.println("Affiche");
        InsertFrame frame = new InsertFrame();
        frame.afficher();
    }
            
}
