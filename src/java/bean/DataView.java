package bean;


import entites.Photo;
import entites.Photographie;
import entites.Sujet;
import entites.Ville;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tpoya
 */
@ManagedBean
@ViewScoped
public class DataView implements Serializable {
     
    private Map<String,List> data;
    
    private Map<String, ArrayList<String>> labels;
    private String cur = "photographies";
    private String filtreVille;

    
    private int nbPhoto;
 

    private int nombreVille;
    private int nombrePhoto;
    Map<String,List> data2;
    Map<String, ArrayList<String>> labels2;

    @PostConstruct
    public void init() {
        List<Photographie> photographies = new ArrayList<>();
        this.filtreVille="Beaugency";
        data = new HashMap();
        labels = new HashMap<>();
        
        data2= new HashMap();
        labels2= new HashMap<>();
        
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/TEA_BD", "postgres", "postgres")) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select P.idarticle, A.discriminant, P.serie, V.nom as ville, S.nom as sujet,P.description,CONCAT(D.jour,' ', D.mois,' ', D.annee) as date,P.note,CONCAT(PE.nom ,' ', PE.prenom,' ', PE.statut) as personne,F.nom as fichier, CL.nombre as nombre_de_cliche, CONCAT(CL.largeur, 'x', CL.longueur) as taille, CL.couleur,CL.neg_inv " +
                    "from photo P, photo_date D, ville V, index_date ID, article A, sujet S, sujet_photo SP, personne PE, fichier F, index_fichier FI, cliche CL " +
                        "where P.id = ID.idphoto and ID.iddate = D.id and V.id = P.ville " +
                        "and A.id = P.idarticle and P.id = SP.idphoto and SP.idsujet = S.id " +
                        "and PE.id = P.personne and P.id = FI.idphoto and FI.idfichier = F.id " +
                        "and CL.id = P.cliche order by P.idarticle;");
            while (resultSet.next()) {
                photographies.add(new Photographie(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10), resultSet.getInt(11), resultSet.getString(12), resultSet.getString(13), resultSet.getString(14)));
                for(Photographie p : photographies){
                }
                labels.put("photographies", (ArrayList<String>) ((Photographie) photographies.get(0)).getFields());
                //System.out.println(labels.get(cur));
                nbPhoto = photographies.size();
            }
            resultSet = statement.executeQuery("select count(distinct ville) from photo");
            while (resultSet.next()) {
                nombreVille=resultSet.getInt("count");
            }
            resultSet = statement.executeQuery("select count(*) from photo");
            while (resultSet.next()) {
                nombrePhoto=resultSet.getInt("count");
            }
        }catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        data.put("photographies", (ArrayList) photographies);
    }

    
    public Map filtrerVille(String ville){
        List<Photographie> photographies2 = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/TEA_BD", "postgres", "postgres")) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select P.idarticle, A.discriminant, P.serie, V.nom as ville, S.nom as sujet,P.description,CONCAT(D.jour,' ', D.mois,' ', D.annee) as date,P.note,CONCAT(PE.nom ,' ', PE.prenom,' ', PE.statut) as personne,F.nom as fichier, CL.nombre as nombre_de_cliche, CONCAT(CL.largeur, 'x', CL.longueur) as taille, CL.couleur,CL.neg_inv " +
                    "from photo P, photo_date D, ville V, index_date ID, article A, sujet S, sujet_photo SP, personne PE, fichier F, index_fichier FI, cliche CL " +
                        "where P.id = ID.idphoto and ID.iddate = D.id and V.id = P.ville " +
                        "and A.id = P.idarticle and P.id = SP.idphoto and SP.idsujet = S.id " +
                        "and PE.id = P.personne and P.id = FI.idphoto and FI.idfichier = F.id " +
                        "and CL.id = P.cliche and upper(V.nom)=upper('"+ ville +"') order by P.idarticle;");
            while (resultSet.next()) {
                photographies2.add(new Photographie(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10), resultSet.getInt(11), resultSet.getString(12), resultSet.getString(13), resultSet.getString(14)));
                labels2.put("FiltreVille", (ArrayList<String>) ((Photographie) photographies2.get(0)).getFields());

            }
        }catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        data2.put("FiltreVille", (ArrayList) photographies2);
        
        return data2;
    }
    
    
    
    public String getFiltreVille() {
        System.out.println(filtreVille);
        return filtreVille;
    }

    public void setFiltreVille(String filtreVille) {
        System.out.println(filtreVille);
        this.filtreVille = filtreVille;
    }

    public int getNombreVille() {
        return nombreVille;
    }

    public void setNombreVille(int nombreVille) {
        this.nombreVille = nombreVille;
    }
    
    
    
    public List getData() {
        return data.get(cur);
    }
    
    public ArrayList<String> getLabels() {
        return labels.get(cur);
    }
    
    public List getData2() {
        return data2.get("FiltreVille");
    }
    
    public ArrayList<String> getLabels2() {
        return labels2.get("FiltreVille ");
    }

    public int getNbPhoto() {
        return nbPhoto;
    }


    public int getNombrePhoto() {
        return nombrePhoto;
    }

    public void setNombrePhoto(int nombrePhoto) {
        this.nombrePhoto = nombrePhoto;
    }
    

    
    
}
