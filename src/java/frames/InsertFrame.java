/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frames;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author tpoya
 */
public class InsertFrame extends JFrame{
    Connection connection;
    JPanel panel;
    JTextField refCindoc,
            serie,
            article,
            discriminant,
            ville,
            sujet,
            description,
            date,
            nBP,
            idxPers,
            nomFichier,
            idxIcono,
            nbCliche,
            tailleCliche,
            remarque;
    
    JComboBox neg_inv,
            c_nb;

    public InsertFrame() {
        this.setLocation(100,100);
        this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        this.setPreferredSize(new Dimension(550, 500));
        JLabel label1 = new JLabel("Recindoc : ");
        JLabel label2 = new JLabel("serie : ");
        JLabel label3 = new JLabel("Article : ");
        JLabel label4 = new JLabel("Discriminant : ");
        JLabel label5 = new JLabel("Ville : ");
        JLabel label6 = new JLabel("Sujet : ");
        JLabel label7 = new JLabel("Description : ");
        JLabel label8 = new JLabel("Date : ");
        JLabel label9 = new JLabel("Note Bas Page : ");
        JLabel label10 = new JLabel("Index Personne : ");
        JLabel label11 = new JLabel("Nom Fichier : ");
        JLabel label12 = new JLabel("Index Iconographique : ");
        JLabel label13 = new JLabel("Nombre de Clichés : ");
        JLabel label14 = new JLabel("Taille des Clichés : ");
        JLabel label15 = new JLabel("Négatif/Inversible : ");
        JLabel label16 = new JLabel("Couleur/Noir-Blanc : ");
        JLabel label17 = new JLabel("Remarque : ");
        
        KeyAdapter ka = new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
              char c = e.getKeyChar();
              if (!((c >= '0') && (c <= '9') ||
                 (c == KeyEvent.VK_BACK_SPACE) ||
                 (c == KeyEvent.VK_DELETE))) {
                getToolkit().beep();
                e.consume();
              }
            }
        };
        
        this.refCindoc = new JTextField();
        refCindoc.addKeyListener(ka);
        this.serie = new JTextField();
        serie.addKeyListener(ka);
        this.article = new JTextField();
        article.addKeyListener(ka);
        this.discriminant = new JTextField();
        this.ville = new JTextField();
        ville.addKeyListener(ka);
        this.sujet = new JTextField();
        sujet.addKeyListener(ka);
        this.description = new JTextField();
        this.date = new JTextField();
        
        this.date.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                String content = date.getText();
                if("".equals(content)) {
                    // On accepte que le champ soit vide donc on ne fait rien
                    return;
                }
                if(content.matches("[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}")) {
                    // Le texte est bon
                    // Ajout du "0" en début de chaine si nécessaire
                    if(content.length() == 11) {
                        date.setText("0" + content);
                    }
                } else {
                    // Le texte n'est pas bon
                    // On efface le texte
                    date.setText("");
                    // Eventuellement afficher un message d'erreur
                }
            }
        });
        this.nBP = new JTextField();
        this.idxPers = new JTextField();
        this.nomFichier = new JTextField();
        this.idxIcono = new JTextField();
        this.nbCliche = new JTextField();
        this.tailleCliche = new JTextField();
        String[] ni = {"negatif", "inversible"};
        this.neg_inv = new JComboBox(ni);
        String[] cn = {"couleur", "nb"};
        this.c_nb = new JComboBox(cn);
        this.remarque = new JTextField();
        
        JButton submit = new JButton("Insérer");
        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                Object source = e.getSource();
 
		if(source == submit){
			inserer();
		}
            }
            
        });
        
        GridLayout gd = new GridLayout(0, 4);
        JPanel form = new JPanel(gd);
        
        panel = new JPanel();
        
        JLabel titre = new JLabel("Insertion");
        panel.add(titre);
        
        form.add(label1);
        form.add(refCindoc);
        form.add(label2);
        form.add(serie);
        form.add(label3);
        form.add(article);
        form.add(label4);
        form.add(discriminant);
        form.add(label5);
        form.add(ville);
        form.add(label6);
        form.add(sujet);
        form.add(label7);
        form.add(description);
        form.add(label8);
        form.add(date);
        form.add(label9);
        form.add(nBP);
        form.add(label10);
        form.add(idxPers);
        form.add(label11);
        form.add(nomFichier);
        form.add(label12);
        form.add(idxIcono);
        form.add(label13);
        form.add(nbCliche);
        form.add(label14);
        form.add(tailleCliche);
        form.add(label15);
        form.add(neg_inv);
        form.add(label16);
        form.add(c_nb);
        form.add(label17);
        form.add(remarque);
        form.add(submit);
        
        panel.add(form);
        
        this.setContentPane(panel);
        
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/TEA_BD", "postgres", "postgres");
        } catch (SQLException ex) {
            Logger.getLogger(InsertFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void afficher() {
        this.pack();
        this.setVisible(true);
    }
    
    public void inserer() {      
        String req = "INSERT INTO photographie VALUES(" + refCindoc + ", " + serie + ", " + article + ", " + discriminant + ", " + ville + ", " + sujet +
                ", " + description + ", " + date + ", " + nBP + ", " + idxPers + ", " + nomFichier + ", " + idxIcono + ", " + nbCliche + ", " + tailleCliche + 
                ", " + neg_inv + ", " + c_nb + ", " + remarque + ")";
        
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(req);
        } catch (SQLException ex) {
            System.err.println("Donnée pas insérée.");
        }
        
    }
    
    
    public static void main(String[] args) {
        InsertFrame f = new InsertFrame();
        f.afficher();
    }
}
